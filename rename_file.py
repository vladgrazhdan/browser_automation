from selenium.webdriver.common.by import By
from vars import *
import time, os, random, string, logging
from notification import notify

logging.basicConfig(filename='run.log', encoding='utf-8', level=logging.DEBUG)

def rename_file(*args):
    rows_number, driver = args[0], args[1]
    page, pages_number = args[2], args[3]
    day, month, year, file_path = args[4], args[5], args[6], args[7]

    # Downloading recordings
    for row in range(1, len(rows_number) + 1):

        print(f'Downloading recording {row}/{len(rows_number)} on page {page}/{pages_number} for the {day} of {month} {year}...')
        element = driver.find_element(By.XPATH, f'//table/tbody/tr[{row}]/td[11]/span')
        driver.execute_script("arguments[0].click();", element)
        time.sleep(5)

        # Retrieving phone call info
        src_number = (driver.find_element(By.XPATH, f'//*[@id="table-arena"]//table/tbody/tr[{row}]/td[1]').text)
        dst_number = (driver.find_element(By.XPATH, f'//*[@id="table-arena"]//table/tbody/tr[{row}]/td[2]').text)
        call_date = (driver.find_element(By.XPATH, f'//*[@id="table-arena"]//table/tbody/tr[{row}]/td[6]').text)
        call_time = (driver.find_element(By.XPATH, f'//*[@id="table-arena"]//table/tbody/tr[{row}]/td[7]').text)
        call_time = call_time[0:2] + call_time[3:5] + call_time[6:8]
        call_duration = (driver.find_element(By.XPATH, f'//*[@id="table-arena"]//table/tbody/tr[{row}]/td[9]').text)
        call_duration = call_duration[0:2] + call_duration[3:5] + call_duration[6:8]
        call_date = call_date[3:5] + call_date[0:2] + call_date[6:10]
        time.sleep(1)

        # Renaming the file
        new_name = f'{src_number}-{dst_number}-{call_date}-{call_time}-{call_duration}.mp3'

        try:
            if not os.path.exists(file_path + '/' + new_name):
                os.rename(download_folder + existing_file_name, file_path + '/' + new_name)
            else:
                letters = string.ascii_lowercase
                os.rename(download_folder + existing_file_name, file_path + '/' + new_name + '-' + ''.join(random.choice(letters) for i in range(5)) + '.mp3')
            time.sleep(3)
        except WindowsError as Argument:
            logging.debug(Argument)
            logging.debug(f'Cannot find the file. Skipping the file {new_name}')
            notify('Windows Error')