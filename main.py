from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from vars import *
from logon import logon
from download_rec import download_rec
from notification import notify
import time
from datetime import datetime
from selenium.webdriver.chrome.service import Service as ChromeService


main_url = input('Please enter the login page URL: ')

service = ChromeService(executable_path=ChromeDriverManager().install())

# Setting up Chrome options to avoid multiple files download pop-up
chrome_options = Options()
chrome_options.add_experimental_option('prefs', {
    'download.prompt_for_download': False,
    'profile.default_content_setting_values.automatic_downloads': 1
    })
    #with webdriver.Chrome(options=chrome_options) as driver:
driver = webdriver.Chrome(options=chrome_options, service=service)

logon(driver,main_url)
driver.get(customer_url)
time.sleep(2)

try:
    download_rec(driver)
except:
    date_time_obj = datetime.now()
    notify('The script failed at ' + date_time_obj.strftime("%H:%M:%S %d-%m-%Y"))

print('Stopping the script...')
driver.quit()