import logging
import boto3
from botocore.exceptions import ClientError
from vars import *

logger = logging.getLogger(__name__)

def notify(notif_message):

    logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')
    sns_resource = boto3.resource(
        'sns',
        aws_access_key_id = aws_access_key_id,
        aws_secret_access_key = aws_secret_access_key,
        region_name = aws_region_name
    )

    message = notif_message

    if sub_phone_number != '':
        print(f"Sending an SMS message directly from SNS to {sub_phone_number}.")

        try:
            response = sns_resource.meta.client.publish(
                PhoneNumber=sub_phone_number, Message=message)
            message_id = response['MessageId']
            logger.info("Published message to %s.", sub_phone_number)
        except ClientError:
            logger.exception("Couldn't publish message to %s.", sub_phone_number)
            raise
        else:
            return message_id