import datetime
from calendar import monthrange

month = 'November'
datetime_object = datetime.datetime.strptime(month, "%B")
print(datetime_object)
month_number = datetime_object.month
print(month_number)
days_number = monthrange(2021, month_number)[1]
print(days_number)