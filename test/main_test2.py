from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from vars import *
from logon import *
from calendar import monthrange
from pathlib import Path
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, os, datetime

# Setting up Chrome options to avoid multiple files download pop-up

chrome_options = Options()
chrome_options.add_experimental_option('prefs', {
    'download.default_directory': f'{working_folder}',
    'download.prompt_for_download': False,
    'profile.default_content_setting_values.automatic_downloads': 1
    })
    #with webdriver.Chrome(options=chrome_options) as driver:
driver = webdriver.Chrome(options=chrome_options)

logon(driver)
driver.get(customer_url)
time.sleep(20)