import os

# Renaming the file
working_folder = r'C:/Users/'
month_list = ['October', 'November']
for month in month_list:
    dir_list = os.listdir(working_folder + month)
    for date in dir_list:
        if len(date) == 4:
            new_date = '0' + date[0] + '0' + date[1:4]
            print(new_date)
        elif len(date) == 5 and int(date[1:3]) in [10,11,12]:
            new_date = '0' + date[0] + date[1:5]
            print(new_date)

        elif len(date) == 5:
            new_date = date[0:2] + '0' + date[2:5]
            print(new_date)
        else:
            new_date = date
            print(new_date)
        os.rename(working_folder + month + '/' + date, working_folder + month + '/' + new_date)