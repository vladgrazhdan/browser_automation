from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from vars import *
from calendar import monthrange
from pathlib import Path
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time, os, datetime

with webdriver.Chrome() as driver:
	# Logging in
	driver.get(main_url)
	time.sleep(10)
#	driver.find_element(By.XPATH, '//input[@name="email"]').send_keys(username)
#	driver.find_element(By.XPATH, '//input[@name="password"]').send_keys(password)
#	driver.find_element(By.XPATH, '//button[@type="button"]').click()
	time.sleep(2)

	# Retrieving call recordings as per the date range
	driver.get(rev_url)
#	for date in date_range:
	date = 'March'
	datetime_object = datetime.datetime.strptime(date, "%B")
	month_number = datetime_object.month
	days_number = monthrange(year, month_number)[1]

		# Creating a folder to store recordings
	if not Path(working_folder + date).exists():
		os.mkdir(working_folder + date)

		# Entering the date range ans selecting recordings only
	driver.find_element(By.XPATH, '//input[@name="start"]').clear()
	driver.find_element(By.XPATH, '//input[@name="start"]').send_keys(f'{month_number}/01/2021' + Keys.ENTER)
	time.sleep(3)
	driver.find_element(By.XPATH, '//input[@name="end"]').clear()
	driver.find_element(By.XPATH, '//input[@name="end"]').send_keys('03/01/2021' + Keys.ENTER)
	time.sleep(3)
	driver.find_element(By.XPATH, '//input[@type="checkbox"]').click()
	time.sleep(3)
	driver.find_element(By.XPATH, '//div[contains(@class,"m-form__actions")]//span[contains(text(),"Search")]').click()
	time.sleep(15)

		# Detecting a number of pages
		#driver.find_element(By.XPATH, '//div[contains(@class,"m--pull-left")]//i[contains(@class,"la-angle-double-right")]/parent::a[contains(@class,"btn-nav")]').click()
		#time.sleep(20)
		#pages_number =  int(driver.find_element(By.XPATH, '//div[contains(@class,"m--pull-left")]//a[contains(@class,"active")]').text)

		#print(f'{pages_number} pages detected')

		# Getting back to page 1
		#driver.find_element(By.XPATH, '//div[contains(@class,"m--pull-left")]//i[contains(@class,"la-angle-double-left")]/parent::a[contains(@class,"btn-nav")]').click()
		#time.sleep(20)

		# Getting a number of recordings per page
		# for page in range(1, pages_number + 1):
		# 	rows_number = driver.find_elements(By.XPATH, '//*[(@id="table-arena")]//table/tbody/tr')
		# 	print(f'{len(rows_number)} recordings on page {page}')
        #
		# 	# Downloading recordings
		# 	for row in range(1, len(rows_number) + 1):
		# 		print(f'Downloading recording {row}')
		# 		#driver.find_element(By.XPATH, f'//table/tbody/tr[{row}]/td[11]/span/i').click()
		# 		element = driver.find_element(By.XPATH, f'//table/tbody/tr[{row}]/td[11]/span/i')
		# 		driver.execute_script("arguments[0].click();", element)
		# 		time.sleep(10)

				# Retrieving phone call info
				#tr_text = (driver.find_element(By.XPATH, f'//*[@id="table-arena"]//table/tbody/tr[{row}]').text)
				#new_text = tr_text.split(' ')
				#new_text = [x for x in new_text if not x[0:2].isalpha()]

				# src_number = (driver.find_element(By.XPATH, f'//*[@id="table-arena"]//table/tbody/tr[{row}]/td[1]').text)
				# dst_number = (driver.find_element(By.XPATH, f'//*[@id="table-arena"]//table/tbody/tr[{row}]/td[2]').text)
				# call_date = (driver.find_element(By.XPATH, f'//*[@id="table-arena"]//table/tbody/tr[{row}]/td[6]').text)
				# call_time = (driver.find_element(By.XPATH, f'//*[@id="table-arena"]//table/tbody/tr[{row}]/td[7]').text)
				# call_time = call_time[0:2] + call_time[3:5] + call_time[6:8]
				# call_duration = (driver.find_element(By.XPATH, f'//*[@id="table-arena"]//table/tbody/tr[{row}]/td[9]').text)
				# call_duration = call_duration[0:2] + call_duration[3:5] + call_duration[6:8]
				# call_date = call_date[3:5] + call_date[0:2] + call_date[6:10]

				#print(new_text)

				# Renaming the file
				#new_name =  f"{new_text[0]}-{new_text[1]}-{new_text[3][3:5]+new_text[3][0:2]+new_text[3][6:10]}-{new_text[4]}-{new_text[5]}.mp3"
				# new_name = f'{src_number}-{dst_number}-{call_date}-{call_time}-{call_duration}.mp3'
				# os.rename(working_folder + existing_name, working_folder + date + '/' + new_name)
				# time.sleep(5)
			# print(f"Leaving page {page}")
	driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
	driver.find_element(By.XPATH, '//*[@id="table-arena"]/div[2]/div[1]/a[4]').click()

	#driver.find_element(By.XPATH, '//div[contains(@class,"m--pull-left")]//i[contains(@class,"la-angle-right")]/parent::a[contains(@class,"btn-nav")]').click()
	time.sleep(120)

driver.quit()