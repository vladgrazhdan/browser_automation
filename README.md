# Chrome browser automation

## Intro

This project was a part of a customer base migration from a CPaaS to another private cloud based platform.
The customer required all sensitive information (call recordings, CDRs etc.) to be quickly and safely migrated 
with minimum effort.

This solution automatically logs on to the platform, navigates to the relevant sections, downloads, renames, and
stores files locally. It also integrates with AWS SNS to send notifications on the migration process.

## Overview

*   [x] **Can be modified and extended**
*   [x] **Tested**

### Requirements

* Python 3.10
* Selenium webdriver 4.0
* AWS account

### Install

Use git to clone this repository. Pre-setup variables in vars.py

### Usage

python main.py
