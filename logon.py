from selenium.webdriver.common.by import By
from vars import *
import time

def logon(*args):

    driver, main_url = args[0],args[1]

    # If no 2FA is required, then logon manually first. After that, no need to enter the creds. Go to the recordings page.
    if len(main_url) < 50:
        driver.get(main_url)
        driver.find_element(By.XPATH, '//input[@name="email"]').send_keys(username)
        driver.find_element(By.XPATH, '//input[@name="password"]').send_keys(password)
        driver.find_element(By.XPATH, '//button[@type="button"]').click()
    else:
        driver.get(main_url)
    time.sleep(2)