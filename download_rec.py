from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from vars import *
from calendar import monthrange
from pathlib import Path
from rename_file import rename_file
import time, os, datetime
from notification import notify

def download_rec(driver):

    for year in date_year:
        if not Path(working_folder + year).exists():
            os.mkdir(working_folder + year)
        for month in date_month:
            datetime_object = datetime.datetime.strptime(month, "%B")
            month_number = datetime_object.month
            days_number = monthrange(int(year), month_number)[1]

            # Creating a folder to store recordings
            if not Path(working_folder + year + '/' + month).exists():
                os.mkdir(working_folder + year + '/' + month)

            # Entering the date range and selecting recordings only
            for day in range(1, days_number + 1):
                print(f'Selecting a new date: the {day} of {month} {year}...')
                driver.find_element(By.XPATH, '//input[@name="start"]').clear()
                driver.find_element(By.XPATH, '//input[@name="start"]').send_keys(f'{month_number}/{day}/{year}' + Keys.ENTER)
                time.sleep(2)
                driver.find_element(By.XPATH, '//input[@name="end"]').clear()
                driver.find_element(By.XPATH, '//input[@name="end"]').send_keys(f'{month_number}/{day}/{year}' + Keys.ENTER)
                time.sleep(2)
                driver.find_element(By.XPATH, '//input[@type="checkbox"]').click()
                time.sleep(2)
                driver.find_element(By.XPATH, '//div[contains(@class,"m-form__actions")]//span[contains(text(),"Search")]').click()
                time.sleep(10)

                # Evaluating a number of recordings per page
                rows_number = driver.find_elements(By.XPATH, '//*[(@id="table-arena")]//table/tbody/tr')
                time.sleep(3)

                # Creating a new file path
                file_path = working_folder + year + '/' + month + '/' + f'{day}{month_number}{year[2:4]}'

                # No recordings on the page
                if len(rows_number) == 1 and driver.find_element(By.XPATH, '//*[(@id="table-arena")]//table/tbody/tr').text == 'No data available.':
                    print(f'No recordings on the {day} of {month} {year}')
                    if not Path(file_path).exists():
                        os.mkdir(file_path)
                    time.sleep(1)
                    print(f'Leaving page 1 for the {day} of {month} {year}...')
                    driver.find_element(By.XPATH, '//input[@type="checkbox"]').click()
                    time.sleep(1)
                # Evaluating a number of pages if the recordings number >= 25
                elif len(rows_number) == 25:
                    driver.find_element(By.XPATH, '//div[contains(@class,"m--pull-left")]//i[contains(@class,"la-angle-double-right")]/parent::a[contains(@class,"btn-nav")]').click()
                    time.sleep(15)
                    pages_number = int(driver.find_element(By.XPATH, '//div[contains(@class,"m--pull-left")]//a[contains(@class,"active")]').text)
                    print(f'{pages_number} pages for day {day}/{days_number} detected')

                    # Getting back to page 1
                    driver.find_element(By.XPATH, '//div[contains(@class,"m--pull-left")]//i[contains(@class,"la-angle-double-left")]/parent::a[contains(@class,"btn-nav")]').click()
                    time.sleep(15)

                    # Creating a folder per date
                    if not Path(file_path).exists():
                        os.mkdir(file_path)

                    # Getting a number of recordings per page
                    for page in range(1, pages_number + 1):
                        rows_number = driver.find_elements(By.XPATH, '//*[(@id="table-arena")]//table/tbody/tr')
                        print(f'{len(rows_number)} recording(s) on page {page}/{pages_number} for the {day} of {month} {year}')
                        time.sleep(2)

                        # Downloading and renaming the files
                        rename_file(rows_number, driver, page, pages_number, day, month, year, file_path)

                        print(f"Leaving page {page}/{pages_number} for the {day} of {month} {year}...")
                        if page < pages_number:
                            driver.find_element(By.XPATH, '//div[@class ="pagination m--pull-left"]//i[@class="la la-angle-right"]/..').click()

                        # if page < pages_number and pages_number == 4:
                        #     driver.find_element(By.XPATH, '//*[@id="table-arena"]/div[2]/div[1]/a[7]').click()
                        #
                        # elif page < pages_number and pages_number == 3:
                        #     driver.find_element(By.XPATH, '//*[@id="table-arena"]/div[2]/div[1]/a[6]').click()
                        #
                        # elif page < pages_number and pages_number == 2:
                        #     driver.find_element(By.XPATH, '//*[@id="table-arena"]/div[2]/div[1]/a[5]').click()
                        #
                        # elif page < pages_number and pages_number == 6:
                        #     driver.find_element(By.XPATH, '//*[@id="table-arena"]/div[2]/div[1]/a[9]').click()
                        #
                        # elif page < pages_number and pages_number == 5:
                        #     driver.find_element(By.XPATH, '//*[@id="table-arena"]/div[2]/div[1]/a[8]').click()
                        #
                        # elif page < pages_number and pages_number >= 7:
                        #
                        #     #driver.find_element(By.XPATH, '//*[@id="table-arena"]/div[2]/div[1]/a[10]').click()
                        #     driver.find_element(By.CLASS_NAME, 'la la-angle-right').click()
                        time.sleep(10)

                    print("Selecting Only show recorded calls")
                    driver.find_element(By.TAG_NAME, 'body').send_keys(Keys.CONTROL + Keys.HOME)
                    time.sleep(2)
                    driver.find_element(By.XPATH, '//input[@type="checkbox"]').click()
                    time.sleep(3)

                # Downloading recordings without evaluating a number of pages if recordings < 25
                else:
                    print(f'There is only one page for the {day} of {month} {year}')
                    # Creating a folder per date
                    if not Path(file_path).exists():
                        os.mkdir(file_path)

                    rename_file(rows_number, driver, 1, 1, day, month, year, file_path)

                    print(f'Leaving page 1 for the {day} of {month} {year}...')
                    driver.find_element(By.XPATH, '//input[@type="checkbox"]').click()
                    time.sleep(1)
            notify(f'{month} {year} completed')