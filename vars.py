import os

# YCT creds
username = os.environ.get('YCT_USER')
password = os.environ.get('YCT_PASSWORD')

# Call recordings URL to browse
customer_url = ''

# Date range
date_year = ['2020', '2021', '2022']
#date_month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
date_month = ['July', 'August', 'September', 'October', 'November', 'December']


# Working folders
working_folder = r'C:/Users/'
download_folder = r'C:/Users/'
existing_file_name = 'recording.mp3'

# AWS
aws_access_key_id = os.environ.get('AWS_ACCESS_KEY_ID')
aws_secret_access_key = os.environ.get('AWS_SECRET_ACCESS_KEY')
sns_topic_name = 'web_automation'
sub_phone_number = ''
aws_region_name = 'ap-southeast-2'